#!/bin/sh

echo "UPDATING MIRRORS"
curl -s "https://archlinux.org/mirrorlist/?country=PL&protocol=https&ip_version=4&use_mirror_status=on" \
    | sed -e 's/^#Server/Server/' -e '/^#/d' > /etc/pacman.d/mirrorlist

### PARU
echo "SETTING UP PARU"
sudo pacman -Sy --needed git
git clone https://aur.archlinux.org/paru.git
cd paru
makepkg -si --noconfirm
cd ..
rm -rf paru

### DEPENDENCIES
echo "INSTALLING DEPENDENCIES"
paru -Sy  --needed --noconfirm $(cat deps)

### .CONFIG ###
echo "COPYING CONFIG FILES"
cp -r   alacritty \
        gammastep \
        hypr \
        nvim \
        qtile \
        redshift \
        waybar \
~/.config/

cp -r   bin \
        .bash_profile \
        .bashrc \
        .xinitrc \
        .themes
~/

cp grub /etc/default/
sudo grub-mkconfig -o /boot/grub/grub.cfg

### LINKS
sudo rm -rf ~/.local/share/Steam
sudo ln -sf /mnt/b/link_targets/Steam ~/.local/share/Steam

ln -sf /mnt/hdd/zdjecia ~/Pictures
ln -sf /mnt/hdd/Wideo ~/Videos
ln -sf /mnt/hdd/dokumenty ~/Documents
ln -sf /mnt/hdd/Muzyka ~/Music
ln -sf /mnt/hdd/pobrane ~/Downloads

cat fstab_tail | sudo tee -a /etc/fstab

