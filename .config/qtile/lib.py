from collections import defaultdict
class config_class():
    def __init__(self):
        return

class command_list():
    GPU_UTIL_COMMAND = "nvidia-smi -q -d UTILIZATION | grep Gpu | awk \"{print \$3}\""


    def __init__(self):
        return

sinks = [
    # "ladspa_output.mbeq_1197.mbeq",
    "alsa_output.usb-GeneralPlus_USB_Audio_Device-00.analog-stereo",
    "alsa_output.pci-0000_00_1f.3.analog-stereo",
]

sinks = defaultdict(lambda: '0')
sinks["alsa_output.usb-GeneralPlus_USB_Audio_Device-00.analog-stereo"] = ''
sinks["alsa_output.pci-0000_00_1f.3.analog-stereo"] = ''

ports = [
    "analog-output-lineout",
    "analog-output-headphones",
]

mic = 'alsa_input.usb-C-Media_Electronics_Inc._USB_PnP_Sound_Device-00.mono-fallback' 

device_indicators = ['', '', '']

network_devices={
        'ethernet' : 'I',
        'wireless'     : 'H',
        'None'     : 'J',
        }

colors = {
        'orange' : '#F0Af16',
        'ored'   : '#F77B53',
        'black'  : '#000000',
        'swamp'  : '#335D03',  
        'lime'   : '#32CD32',
        }


gamma_rules={
        'Counter-Strike 2' :                1.4,
        'League of Legends (TM) Client':    1.0,
        'MultiMC: 1.4.7':                   1.1,
}

gamma_classes={
        'Minecraft'     : 1.1,
        'tf_linux64'    : 1.1,
}

WALLPAPER1 = '~/Pictures/wallpaper/obrazy/landscape_cropped.png'
WALLPAPER2 = '~/Pictures/wallpaper/jager.png'

curr_gamma = 1

comp = True
bars = True
walp = True

RES_CUSTOM = '2560x1440'
RES_NORMAL = '3440x1440'
SCALE = False

SCREEN_MAIN = 'DP-4'
SCREEN_SEC  = 'DP-2'
SCREE_TV = 'HDMI-0'

RES_SECONDARY = '1920x1080'
RES_TV = '1920x1080'

RES = True
#RES_CUSTOM = f'nvidia-settings -a CurrentMetaMode="DP-4: 3440x1440_100 @{RES_CUSTOM} +0+0 {{ViewPortIn={RES_CUSTOM}, ViewPortOut={[RES_CUSTOM, RES_NORMAL][SCALE]}+0+0, ForceCompositionPipeline=Off, ForceFullCompositionPipeline=Off}}"'
#RES_NORMAL = 'nvidia-settings -a CurrentMetaMode="DP-4: 3440x1440_100 @3440x1440 +0+0 {ViewPortIn=3440x1440, ViewPortOut=3440x1440+0+0, ForceCompositionPipeline=On, ForceFullCompositionPipeline=On}"'
RES_SECONDARY= f'xrandr --output {SCREEN_SEC} --mode {RES_SECONDARY} --rate 75 --right-of {SCREEN_MAIN} --rotate left'
#RES_SECONDARY=f'xrandr --output HDMI-0 --off'
DPI_COMMAND = 'xrandr --dpi 96'

commands = command_list()

