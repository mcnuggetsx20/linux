#!/bin/sh

/home/mcnuggetsx20/.config/qtile/scripts/audio &

xinput --set-prop "Microsoft Microsoft® Nano Transceiver v2.0 Mouse" "Coordinate Transformation Matrix" 1.7 0 0 0 1.7 0 0 0 1
xinput --set-prop "pointer:BenQ ZOWIE BenQ ZOWIE Gaming Mouse" "Coordinate Transformation Matrix" 0.65 0 0 0 0.65 0 0 0 1 

redshift -c ~/.config/redshift/rc.conf -o
redshift -c ~/.config/redshift/rc2.conf -o

easyeffects --gapplication-service &
xsetroot -cursor_name left_ptr &

#xrdb ~/.config/xterm/hack
#xterm -e 'unimatrix -g=black -s 96' & sleep .8s && transset -n hack 0.5
#
#xrdb ~/.config/xterm/default

