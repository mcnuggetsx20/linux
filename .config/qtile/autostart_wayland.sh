#!/bin/sh
wlr-randr --output DP-3 --mode 3440x1440@99.982Hz
wlr-randr --output DP-2 --mode 1920x1080@74.973Hz --right-of DP-3 --transform 90
wlr-randr --output HDMI-A-1 --off
