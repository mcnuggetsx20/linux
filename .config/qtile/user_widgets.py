from libqtile import widget, qtile, bar
from libqtile.lazy import lazy
from qtile_extras import widget as ewidget
from qtile_extras.widget.groupbox2 import GroupBoxRule, ScreenRule
from funx import *
from colors import *

widget_defaults = dict(
    font='Roboto Mono Regular',
    fontsize=16,
    inactive='#FFFFFF',
    padding=0,
    background = black,
    foreground = gray,
    direction = 'ttb',
    rotate=False,
)

#nie dawac przecinka po widgetach bo wtedy python mysli ze widget jest tuplem (wtf)

# w groupboxie trzeba powtorzyc ustawienie czcionki
# bez specyfikacji rodzaju (bold/medium itp) bo 
# groupbox umie tylko bez niej czytac (???) a w innym 
# wypadku ustawia na domyslna "sans"
groupbox = widget.GroupBox(
    highlight_method='block', 
    this_current_screen_border= yellow, 
    this_screen_border=red,
    other_screen_border = dgreen,
    block_highlight_text_color=black,
    inactive=gray,
    active=gray,
    highlight_color = gray,
    foreground=gray,
    disable_drag=True,
    use_mouse_wheel=False,
    font="Roboto Mono Light",
    markup=True,
    fmt="<b>{}</b>",
    direction ='ttb',
)

groupbox2 = ewidget.GroupBox2(
    rules =[GroupBoxRule(block_colour=yellow).when(screen=ScreenRule.THIS), 

            GroupBoxRule(block_colour=dgreen).when(screen=ScreenRule.OTHER), 
            GroupBoxRule(text_colour=black).when(screen=ScreenRule.OTHER), 

            GroupBoxRule(text_colour=black).when(focused=True), 
            GroupBoxRule(text_colour=gray).when(focused=False)
    ]
)


defSpacer = widget.Spacer(12)
stretcher = widget.Spacer(bar.STRETCH)
defSpace = 12

homeIcon = widget.TextBox(
                    text = ' Arch',
                    background=dblue,
                    foreground=black,
                    padding=-2,
                )

mainNotifier = ewidget.StatusNotifier(
                    highlight_colour=dviolet,
                    menu_foreground = gray,
                    #menu_background = dgray,
                    menu_font = widget_defaults['font'],
                    menu_fontsize = widget_defaults['fontsize'],
                    menu_row_height = 11,
                    icon_size=18,
                    padding=5,
                )


mainTaskList = widget.TaskList(
                    parse_text=lambda text: '|' + text, 
                    max_title_width=1000,
                    borderwidth=0, 
                    border=black,
                    icon_size=18, 
                    txt_floating='',
                    spacing = 20,
                    foreground = gray,
                    background = '#3f3e37',
                    txt_minimized='-',
                    # font='csgofont',
                    # fontsize=15,
                    #padding_y=-4,
                )


currentWindowName = widget.TextBox(
                    name='current window',
                    foreground = gray,
                    font='Samsung Sans Bold',
                    #fontsize=14,
                    #background = gray,
                    #max_chars=20,
                )

debug = widget.TextBox(
                    name='debug',
                    text='dbg',
                )

weather = widget.GenPollText(
                    max_chars=60,
                    name = 'weather1',
                    func = wttr('Wojnów'),
                    font='csgofont',
                    fontsize = 15,
                    update_interval=600,
                    background = dgray,
                    foreground = gray,
                )

gpuUtilization = widget.GenPollText(
                    name='gpu_util',
                    func = lambda : "GPU UTIL: " + check_output(commands.GPU_UTIL_COMMAND, shell=True, encoding='utf-8')[:-1] + '%',
                    font='csgofont',
                    fontsize = 15,
                    update_interval=2,
                    background = dgray,
                    foreground = gray,
                )

currentLayout = widget.CurrentLayout(
                    background = '#121210',
                    foreground = gray,
                )

mainClock = widget.Clock(
    format= f'<span background="{gray}" foreground="{black}">{' '*4}{' '*4}</span>\n'
            f'<span weight="bold">%d.%m.%y\n%H:%M:%S</span>',
    markup = True,
    # fontsize = widget_defaults['fontsize'] -2,
    font = 'csgofont',
)



