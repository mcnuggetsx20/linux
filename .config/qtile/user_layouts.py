from libqtile import layout
from colors import *
from libqtile.config import Click, Drag, Group, Key, Match, Screen

all_layouts = [
    layout.MonadThreeCol(
        border_focus=orange, 
        border_width=2, 
        single_border_width=2, 
        margin=[30, 40, 40, 40],
        new_client_position='before_current', 
        change_ratio=0.025,
        min_ratio=0,
        ratio=0.48,
        single_margin=[20, 20, 20, 20],
    ),

    # layout.MonadThreeCol(
    #     border_focus=orange, 
    #     border_width=2, 
    #     single_border_width=0, 
    #     margin=0,
    #     new_client_position='before_current', 
    #     change_ratio=0.025,
    #     min_ratio=0,
    #     ratio=0.48,
    #     single_margin=0,
    # ),

    layout.Max(border_width=0, border_focus='#000000', margin=[0, 0, 0, 0]),
    # layout.Columns(
    #     border_focus=orange, 
    #     border_normal='#000000',
    #     border_width=2, 
    #     single_border_width=0, 
    #     margin=0,
    #     new_client_position='before_current', 
    #     min_ratio=0,
    #     single_margin=0,
    #     num_columns=3,
    # ),
]
floating_layout = layout.Floating(
        border_width=0,
        border_focus=green,
        float_rules=[
            # Run the utility of `xprop` to see the wm class and name of an X client.
            *layout.Floating.default_float_rules,
            Match(wm_class='confirmreset'),  # gitk
            Match(wm_class='makebranch'),  # gitk
            Match(wm_class='maketag'),  # gitk
            Match(wm_class='ssh-askpass'),  # ssh-askpass
            Match(wm_class='polo'),  # ssh-askpass
            Match(title='branchdialog'),  # gitk
            Match(title='pinentry'),  # GPG key password entry
            Match(wm_class='feh'),
            #Match(wm_class='pavucontrol'),
            Match(title='Straw'),
            Match(wm_class='clementine'),
            Match(wm_class='python3.10'),
            Match(title='server-console'),
        ]
)

gaming_layout = layout.Floating(
    border_width=0,
)

