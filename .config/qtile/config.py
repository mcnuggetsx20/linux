from typing import List  # noqa: F401
from libqtile import bar, layout, widget, qtile, hook
from libqtile.lazy import lazy
from libqtile.config import Click, Drag, Group, Key, Match, Screen
#from libqtile.command import lazy
from libqtile.command.client import InteractiveCommandClient
from funx import *
from lib import *
from subprocess import call, Popen, check_output
import os
import user_widgets
from colors import *
import user_layouts
from qtile_extras import widget as ewidget
from qtile_extras.widget.groupbox2 import GroupBoxRule, ScreenRule

iconPath = '~/.config/qtile/icons/'
BLK=False

################### Hooks #########################
@hook.subscribe.startup_once
def autostart():
    home = os.path.expanduser('~')
    qtile.groups_map['misc'].cmd_toscreen(1)
    qtile.groups_map['tv'].cmd_toscreen(2)
    call([home + '/.config/qtile/autostart.sh'])
    #call([home + '/.config/autostart_wayland.sh'])
    return

@hook.subscribe.startup
def start():
    call(['xsetroot', '-cursor_name', 'left_ptr'])
    return

@hook.subscribe.client_focus
def newFocus(window):
    global CSGO, BLK
    #qtile.widgets_map['debug'].update(window.cmd_get_position())
    qtile.widgets_map['current window'].update(window.name)
    #qtile.widgets_map['debug'].update(str(curr_gamma))

    gammaGaming(window)

    if window.name == 'blackout' and not BLK:
        window.cmd_set_position_floating(0,0)
        window.cmd_disable_floating()
        BLK=True

@hook.subscribe.client_name_updated
def nameUpdate(window):
    qtile.widgets_map['current window'].update(window.name)

    #gammaGaming(window.name)

@hook.subscribe.client_killed
def killed(zombie):
    global CSGO, curr_gamma
    #_id = str(zombie.info()['id'])
    #Popen('rm ' + iconPath + _id + '*', shell=True)
    qtile.widgets_map['current window'].update('None')

    if curr_gamma != 1:
        brightness_toggle(1)
        curr_gamma = 1

def change(name):
    qtile.widgets_map['steam'].background='#FFFFFF'
    qtile.widgets_map['steam'].draw()

#################### Variables #########################
mod = "mod1"
sup = "mod4"
terminal = "alacritty -e nvim --cmd term -c 'set ma' -c startinsert"
bar_indent=7
this_dir = '/home/mcnuggetsx20/.config/qtile/'
bin_dir = '/home/mcnuggetsx20/bin/'
dmenu_options = f'-sb "#4db5bd" -nf "#000000" -sf "#C53AF0" -nb "#d5d1c5" -c -l 30'


################## Keybinds #########################

keys = [
    #My stuff
    Key([sup], 'b', lazy.spawn('brave --password-store=basic')),
    #Key([mod], 'p', lazy.spawn("dmenu_run -sb '" + green + "' -nf '" + violet + "' -sf '" + black + "' -nb '" + black + "'")),
    Key([mod], 'p', lazy.spawn(f'dmenu_run {dmenu_options}')),
    Key([mod], 'o',  lazy.spawn(f'j4-dmenu-desktop --dmenu \'dmenu {dmenu_options} -i\'')),
    Key([sup], 'f', lazy.spawn('pcmanfm')),
    Key([sup], 'm', lazy.spawn(terminal + ' -e htop')),
    Key([mod], 'r', lazy.to_screen(2)),
    Key([mod], 'e', lazy.to_screen(1)),
    Key([mod], 'w', lazy.to_screen(0)),
    Key([sup], 'p', lazy.spawn('feh /mnt/hdd/zdjecia/plan_lekcji.png')),
    Key([sup], 'q', lazy.spawn('sh power_menu')),
    Key([sup], 'c', lazy.function(compswitch)),
    Key([sup], 'r', lazy.function(resSwitch)),
    #Key([sup], 't', lazy.function(change)),
    #Key([],    'XF86MonBrightnessDown', lazy.function(brightness_toggle())),

    #Volume Control
    Key([],    'XF86AudioRaiseVolume', lazy.spawn('pamixer -i 5')),
    Key([],    'XF86AudioLowerVolume', lazy.spawn('pamixer -d 5')),
    Key([],    'XF86AudioMute', lazy.spawn('pulsemixer --toggle-mute')),

    Key([sup], 'XF86AudioRaiseVolume', lazy.function(mic_vol_change(True))),
    Key([sup], 'XF86AudioLowerVolume', lazy.function(mic_vol_change(False))),
    Key([sup], 'XF86AudioMute', lazy.spawn('pulsemixer --toggle-mute')),
    Key([sup],  'v', lazy.spawn('pavucontrol')),


    Key([mod], 'F1', lazy.function(fanSpeed(False))),
    Key([mod], 'F2', lazy.function(fanSpeed(True))),

    Key([sup], 'bracketleft', lazy.spawn('Straw')),

    Key([mod], 'a', lazy.spawn('/home/mcnuggetsx20/bin/change_audio_sink')),
    Key([mod, 'shift'], 's', lazy.spawn(bin_dir + 'screenshot -s')),
    Key([], 'Print', lazy.spawn(bin_dir + 'screenshot')),
    Key([],    'XF86HomePage', lazy.spawn('brave')),
    Key([mod], "f", lazy.window.toggle_fullscreen(), desc="Toggle fullscreen on the focused window",),
    Key([mod], 't', lazy.window.toggle_floating()),
    Key([sup], 'j', lazy.window.toggle_minimize()),
    Key([mod], 'Tab', lazy.spawn('rofi -show window')),

    # Switch between windows
    Key([mod], "h", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "l", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "j", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "k", lazy.layout.up(), desc="Move focus up"),
    Key([mod], "space", lazy.layout.next(),
        desc="Move window focus to other window"),
    Key([mod], "s", lazy.group.prev_window()),
    Key([mod], "d", lazy.group.next_window()),

    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key([mod, "shift"], "h", lazy.layout.shuffle_left(),
        desc="Move window to the left"),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right(),
        desc="Move window to the right"),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down(),
        desc="Move window down"),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(), desc="Move window up"),

    # Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink.
    Key([mod, "control"], "h", lazy.layout.grow_left(),
        desc="Grow window to the left"),
    Key([mod, "control"], "l", lazy.layout.grow_right(),
        desc="Grow window to the right"),
    Key([mod, "control"], "j", lazy.layout.grow_down(),
        desc="Grow window down"),
    Key([mod, "control"], "k", lazy.layout.grow_up(), desc="Grow window up"),
    Key([mod], "n", lazy.layout.normalize(), desc="Reset all window sizes"),
    Key([mod], 'i', lazy.layout.grow()),
    Key([mod], 'm', lazy.layout.shrink()),

    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key([mod, "shift"], "Return", lazy.layout.toggle_split(),
        desc="Toggle between split and unsplit sides of stack"),
    Key([sup], "Return", lazy.spawn(terminal), desc="Launch terminal"),

    # Toggle between different layouts as defined below
    Key([mod], "tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([sup], "BackSpace", lazy.window.kill(), desc="Kill focused window"),

    Key([mod, "control"], "r", lazy.restart(), desc="Restart Qtile"),
    Key([mod, "control"], "q", lazy.shutdown(), desc="Shutdown Qtile"),
    # Key([mod], "r", lazy.spawncmd(),
    #     desc="Spawn a command using a prompt widget"),
]

################### Layouts #########################
all_layouts = user_layouts.all_layouts
gaming_layout = user_layouts.gaming_layout
floating_layout = user_layouts.floating_layout


################### Groups #########################

groups = [
    Group(
        #name='', 
        name='master',
        position=1, 
        layouts=all_layouts
    ),

    Group(
        #name='', 
        name='slave',
        position=2, 
        layouts=all_layouts
    ),

    Group(
        #name='',
        name='dev',
        position=3, 
        layouts=all_layouts
    ),

    Group(
        #name='',
        name='etc',
        position=4, 
        layouts=all_layouts, 
    ),
        
    Group(
        #name='', 
        name='noob',
        position=5, 
        layouts=[gaming_layout],
        matches = [
            Match(wm_class='csgo_linux64'),
            Match(title='Minecraft* 1.18.2'),
            Match(title='LEGO® Star Wars™: The Complete Saga'),
            #Match(wm_class='hl2_linux'),
            Match(wm_class='Steam'), 
            Match(wm_class='steam'), 
            Match(wm_class='MultiMC'), 
            Match(wm_class='leagueclientux.exe'),
            Match(title='Wine System Tray'),
        ]
    ),
    Group(
        name='misc',
        #name='6',
        position=6,
        layouts=[layout.Columns(
            num_columns=1,
            border_focus=orange,
            border_width=1,
            insert_position=0,
            margin_on_single=[10,20,10,20],
            margin=[10,25,10,25],
            ),
        layout.Max(border_width=0, border_focus='#000000', margin=[0, 0, 0, 0]),
        ],
        matches = [
            Match(wm_class='discord'),
            Match(wm_class='python3.10'),
            Match(wm_class='caprine'),
            Match(wm_class='signal'),
            Match(wm_class='TeamSpeak 3'),
            Match(wm_class='crx_ldaljpfodoglinmbbpgohnfikcdphfhj'), #Messenger
        ],
    ),

    Group(
        name='tv',
        #name='6',
        position=7,
        layouts=[layout.Columns(
            num_columns=1,
            border_focus=orange,
            border_width=1,
            insert_position=0,
            margin_on_single=[10,50,10,50],
            margin=[10,25,10,25],
            ),
        layout.Max(border_width=0, border_focus='#000000', margin=[0, 0, 0, 0]),
        ],
        matches = [
        ],
    ),
]

for i in groups:
    keys.extend([
        # mod1 + letter of group = switch to group
        #Key([mod], str(i.position), lazy.group[i.name].toscreen()),
        Key([mod], str(i.position), lazy.function(groupSwitch(i))),
        # mod1 + shift + letter of group = switch to & move focused window to group
        Key([mod, "shift"], str(i.position), lazy.window.togroup(i.name)),
        # Or, use below if you prefer not to switch to that group.
        # # mod1 + shift + letter of group = move focused window to group
        # Key([mod, "shift"], i.name, lazy.window.togroup(i.name),
        #     desc="move focused window to group {}".format(i.name)),
    ])

################### Screens and widgets#########################

widget_defaults = dict(
    font='csgofont',
    fontsize=17,
    inactive='#FFFFFF',
    padding=0,
    background = black,
    foreground = gray,
    direction = 'ttb',
    rotate=False,
)
space = 12

screens = [
    Screen(
        wallpaper=WALLPAPER1,
        wallpaper_mode='center',
        #top1
        left=bar.Bar(
            margin=[15, 0, 975, 7], #[N, E, S, W]
            #background='#1b1919.90',
            background=black,
            size=70,
            border_width=3,
            border_color=black,
            widgets= [

                widget.GenPollText(
                    func = lambda: '-'.join(check_output(['uname', '-r'], text=True).strip().split('-')[:2]),
                    fmt =   f'<span background="{blue}" foreground="{black}">'
                            f'{' '*1} Arch{' '*3}</span>'
                            '\n{}' ,
                    padding=-2,
                    interval=None,
                    foreground=blue,
                ), widget.Spacer(space),

                widget.CurrentLayout(
                    background = '#121210',
                    foreground = white,
                    markup=True,
                    fmt='<span weight="bold">[{}]</span>',
                    max_chars = 4,
                ),

                ewidget.GroupBox2(
                    rules =[
                        GroupBoxRule(block_colour=yellow).when(screen=ScreenRule.THIS), 
                        GroupBoxRule(text_colour=yellow).when(screen=ScreenRule.OTHER), 
                        GroupBoxRule(text_colour=black).when(focused=True), 
                        GroupBoxRule(text_colour=gray).when(focused=False),
                    ],
                ), widget.Spacer(space),

                widget.Clock(
                    format= f'<span background="{gray}" foreground="{black}">{' '*4}{' '*4}</span>\n'
                            f'<span weight="bold">%A\n%d.%m.%y\n%H:%M:%S</span>',
                    markup = True,
                ), widget.Spacer(space),

                widget.TextBox(
                    foreground = gray,
                    background = purple,
                    text = ''
                ),
                widget.GenPollText(
                    func=DiskSpace,
                    foreground = violet,
                    visible_on_warn =False,
                    interval=2,
                    scroll=True,
                    scroll_delay=1,
                    scroll_interval=0.05,
                ),widget.Spacer(space),

                widget.GenPollText(
                    func = keyboard_battery,
                    interval = 2,
                    fmt= f'<span background="{orange}" foreground="{black}">{' '*4}{' '*4}\n</span>'
                        '{}',
                    foreground = ored,
                ), widget.Spacer(space),

                widget.TextBox(
                    name = 'audio volume',
                    text =  f'{sinks[(check_output(['pactl', 'get-default-sink'], text=True).strip())]}: '
                            f'{check_output(['pamixer', '--get-volume'], text=True).strip()}%'
                ),

                # user_widgets.mainNotifier,
                # user_widgets.homeIcon,
            ],    
        ),

    ),

    Screen(
        wallpaper=WALLPAPER2,
        wallpaper_mode='fill',

        #TOP2
        top=bar.Bar(
            margin=[0, 10, 0, 10], #[N, E, S, W]
            background = '#000000.50',
            size=20,
            widgets=[
                # user_widgets.groupboxScreen1,
                user_widgets.mainNotifier,
                # user_widgets.debug,
                # user_widgets.stretcher,
                # user_widgets.weather,
                # user_widgets.defSpacer,
                # user_widgets.gpuUtilization,
                # *user_widgets.date2,
                # *user_widgets.time2,
            ],
        ),
    ),

    # Screen(
    #     wallpaper = WALLPAPER1,
    #     wallpaper_mode='fill',
    #
    #     #TV BAR
    #     # bottom=bar.Bar(
    #     #     margin = 0,
    #     #     background = black,
    #     #     size = 40,
    #     #     widgets = [
    #     #         user_widgets.groupboxScreen3,
    #     #         user_widgets.stretcher,
    #     #         *user_widgets.date3,
    #     #         user_widgets.defSpacer,
    #     #         *user_widgets.time3,
    #     #     ],
    #     # ),
    # ),
]

############ Miscellaneous #############

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(), start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
follow_mouse_focus = True
bring_front_click = True
cursor_warp = False
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not? 
auto_minimize = False

wmname ="LG3D"
