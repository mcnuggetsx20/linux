vim.cmd [[command WW silent! :w !sudo tee %]]

vim.api.nvim_create_user_command(
    'CustomChangeDirectory',
    function(opts)
        vim.cmd('lcd ' .. opts.args .. '')
        vim.cmd('echo "' .. opts.args .. '"')
        local api = require('nvim-tree.api')
        api.tree.change_root(opts.args)
    end,
    {nargs = 1}
)

vim.api.nvim_create_user_command(
    'ExecuteToTerminal',
    function(opts)
        ExecuteToTerminal(opts.args)
    end,
    {nargs = '*'}
)

vim.api.nvim_create_user_command(
    'ExecuteToWindow',
    function(opts)
        ExecuteToWindow(opts.args)
    end,
    {nargs = '*'}
)

vim.api.nvim_create_user_command('SignatureHelp', function()
    vim.lsp.buf.signature_help()
end, {})
