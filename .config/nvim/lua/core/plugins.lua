local ensure_packer = function()
  local fn = vim.fn
  local install_path = fn.stdpath('data')..'/site/pack/packer/start/packer.nvim'
  if fn.empty(fn.glob(install_path)) > 0 then
    fn.system({'git', 'clone', '--depth', '1', 'https://github.com/wbthomason/packer.nvim', install_path})
    vim.cmd [[packadd packer.nvim]]
    return true
  end
  return false
end

local packer_bootstrap = ensure_packer()

return require('packer').startup(function(use)
    use 'wbthomason/packer.nvim'
    use 'nvim-treesitter/nvim-treesitter'
    use 'nvim-treesitter/playground'
    use 'nvim-tree/nvim-web-devicons'
    use 'nvim-tree/nvim-tree.lua'

    -- color themes
    use {
        'ayu-theme/ayu-vim',
        'blueshirts/darcula',
        'lifepillar/vim-solarized8',
        'NLKNguyen/papercolor-theme',
        'Abstract-IDE/Abstract-cs',
        'olimorris/onedarkpro.nvim',
        'catppuccin/nvim'
    }

    -- lsp
    use {
        'williamboman/mason.nvim',
        'williamboman/mason-lspconfig.nvim',
        'neovim/nvim-lspconfig',
        'williamboman/nvim-lsp-installer',
        'mfussenegger/nvim-jdtls',
        'mrcjkb/rustaceanvim',
        'scalameta/nvim-metals',
        'nvim-lua/plenary.nvim',
        'mfussenegger/nvim-dap',
        'ray-x/lsp_signature.nvim',
    }

    -- completion
    use {
        'nvim-lua/completion-nvim',
        'hrsh7th/nvim-cmp',
        'hrsh7th/cmp-nvim-lsp',
        'hrsh7th/cmp-buffer',
        'hrsh7th/cmp-path',
        'hrsh7th/cmp-cmdline',
        'hrsh7th/cmp-vsnip',
        'hrsh7th/vim-vsnip',
        'hrsh7th/vim-vsnip-integ',
        'L3MON4D3/LuaSnip',
        "saadparwaiz1/cmp_luasnip",
        "rafamadriz/friendly-snippets",
        "windwp/nvim-autopairs",
        "windwp/nvim-ts-autotag",
    }

    --other
    use{
        'brenoprata10/nvim-highlight-colors',
        'numToStr/Comment.nvim',
        "ziontee113/color-picker.nvim",
        "kylechui/nvim-surround",
    }

    use {
        'fei6409/log-highlight.nvim',
        config = function()
            require('log-highlight').setup {}
        end,
    }

    use {
      'nvim-telescope/telescope.nvim', tag = '0.1.8',
    -- or                            , branch = '0.1.x',
      requires = { {'nvim-lua/plenary.nvim'} }
}

  -- My plugins here
  -- use 'foo1/bar1.nvim'
  -- use 'foo2/bar2.nvim'

  -- Automatically set up your configuration after cloning packer.nvim
  -- Put this at the end after all plugins
  if packer_bootstrap then
    require('packer').sync()
  end
end)
