local cmp = require("cmp")

require("luasnip.loaders.from_vscode").lazy_load()

cmp.setup({
   mapping = cmp.mapping.preset.insert({
      ['<C-b>'] = cmp.mapping.scroll_docs(-4),
      ['<C-f>'] = cmp.mapping.scroll_docs(4),
      ['<C-o>'] = cmp.mapping.complete(),
      ['<C-e>'] = cmp.mapping.abort(),
      ['<CR>'] = cmp.mapping.confirm({ select = true }),
    }),
  sources = cmp.config.sources({
    { name = 'nvim_lsp' },
    { name = 'luasnip' },
    { name = 'nvim_lsp_signature_help' },
  }, {
    { name = 'buffer' },
  }), 
  snippet = {
      expand = function(args)
        vim.fn["vsnip#anonymous"](args.body)
      end,
    },
})

-- Mappings for vim-vsnip
vim.api.nvim_set_keymap('i', '<C-j>', "vsnip#expandable()  ? '<Plug>(vsnip-expand)'         : '<C-j>'", { expr = true })
vim.api.nvim_set_keymap('s', '<C-j>', "vsnip#expandable()  ? '<Plug>(vsnip-expand)'         : '<C-j>'", { expr = true })

-- Jump forward or backward
vim.api.nvim_set_keymap('i', '<C-l>', "vsnip#available(1)  ? '<Plug>(vsnip-jump-next)'      : '<C-l>'", { expr = true })
vim.api.nvim_set_keymap('s', '<C-l>', "vsnip#available(1)  ? '<Plug>(vsnip-jump-next)'      : '<C-l>'", { expr = true })
vim.api.nvim_set_keymap('i', '<C-h>', "vsnip#available(-1) ? '<Plug>(vsnip-jump-prev)'      : '<C-h>'", { expr = true })
vim.api.nvim_set_keymap('s', '<C-h>', "vsnip#available(-1) ? '<Plug>(vsnip-jump-prev)'      : '<C-h>'", { expr = true })
