-- disable netrw at the very start of your init.lua
-- vim.g.loaded_netrw = 1
-- vim.g.loaded_netrwPlugin = 1
vim.g.nvim_tree_disable_netrw = false
vim.g.nvim_tree_hijack_netrw = true

-- optionally enable 24-bit colour
vim.opt.termguicolors = true

-- empty setup using defaults
-- require("nvim-tree").setup()

-- OR setup with some options
require("nvim-tree").setup({
    sync_root_with_cwd = true,
    respect_buf_cwd = false,
    update_focused_file = {
        enable = true,
        update_root = true,
    },

    tab ={
        sync ={
            open = true,
            close = false,
            ignore = {},
        },
    },

    sort = {
        sorter = "case_sensitive",
    },

    view = {
        width = 30,
    },

    renderer = {
        group_empty = true,
    },

    filters = {
        dotfiles = false,
    },

    log = {
        enable = true,
        types = {
          all = true,
        },
        truncate = true,
    },
})

local api = require'nvim-tree.api'
vim.cmd([[
    :hi      NvimTreeExecFile    guifg=DarkMagenta
    :hi      NvimTreeSpecialFile guifg=#ff80ff gui=underline
    :hi      NvimTreeSymlink     guifg=Yellow  gui=italic
    :hi link NvimTreeImageFile   Title
]])


-- vim.api.nvim_create_autocmd({"TabEnter", "VimEnter"}, {
--     callback = function()
--         -- if not api.tree.is_visible() then
--             api.tree.open()
--             vim.cmd('wincmd p')
--         -- end
--     end,
--     desc = 'Ensures nvim-tree is visible across all tabs',
-- })
--
-- vim.api.nvim_create_autocmd("BufEnter", {
--     callback = function()
--         api.tree.reload()
--     end,
--     desc = 'Ensures nvim-tree is visible across all tabs',
-- })

api.events.subscribe("TreeOpen", function()
    vim.wo.statusline = '%#normal#'
end)

-- vim.api.nvim_create_autocmd("QuitPre", {
--     callback = function()
--         api.tree.close()
--     end,
--     desc = 'Closes nvim-tree on :q'
-- })
