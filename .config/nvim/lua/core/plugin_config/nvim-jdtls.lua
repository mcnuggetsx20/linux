local config = {
  cmd = {'jdtls'}, -- adjust the path to your jdt-language-server executable
  root_dir = vim.fs.dirname(vim.fs.find({'gradlew', '.git', 'mvnw'}, { upward = true })[1]), -- infer the project root directory
}

require('jdtls').start_or_attach(config)
