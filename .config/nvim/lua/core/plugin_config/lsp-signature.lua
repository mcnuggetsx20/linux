require'lsp_signature'.setup {
    bind = true, -- This is mandatory, otherwise border config won't get registered
    handler_opts = { border = "rounded" }, -- Configure the border of the signature help window
    always_trigger = false, -- Optional: set to true if you want to always show the signature help
}
