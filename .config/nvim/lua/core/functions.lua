function ExecuteToWindow(command)
  --sprawdzamy czy taki bufor juz istnieje zeby go w razie w wyjebac
  local buf_title = command
  local existing_bufnr = vim.fn.bufnr(buf_title)
  if existing_bufnr ~= -1 and vim.fn.bufexists(existing_bufnr) == 1 then
    -- Bufor już istnieje, przełącz się do niego
    vim.api.nvim_buf_delete(existing_bufnr, { force = true })
    print('Buffer "output" already exists. Shell command ' .. command .. ' executed.')
  end

  local output = vim.fn.systemlist(command) --output komendy
  --vim.cmd('botright 80 vnew') --tworzenie okna
  vim.cmd('bel 10 new') --tworzenie okna

  local winnr = vim.api.nvim_get_current_win() --po poprzedniej komendzie wjechalismy do nowego okna
  local bufnr = vim.api.nvim_win_get_buf(winnr) --zdobywanie bufora w tym nowym oknie

  --jakies luzne ustawienia bufora
  vim.api.nvim_buf_set_option(bufnr, 'swapfile', false)
  vim.api.nvim_buf_set_option(bufnr, 'filetype', 'shelloutput')
  vim.api.nvim_buf_set_option(bufnr, 'buftype', 'nofile')
  vim.api.nvim_buf_set_option(bufnr, 'bufhidden', 'wipe')

  vim.api.nvim_buf_set_name(bufnr, buf_title) --zmiana nazwy bufora
  vim.api.nvim_buf_set_lines(bufnr, 0, -1, false, output) --wstawienie wyniku komendy do okna

  vim.cmd('wincmd k')
end

function ExecuteToTerminal(command)

  -- Przeszukujemy wszystkie bufory
  for _, win in ipairs(vim.api.nvim_list_wins()) do
    local buf = vim.api.nvim_win_get_buf(win)
    local buftype = vim.api.nvim_buf_get_option(buf, "buftype")

    -- Jeśli bufor to terminal, wysyłamy tam komendę
    if buftype == "terminal" then
      vim.fn.chansend(vim.api.nvim_buf_get_var(buf, "terminal_job_id"), command .. "\n")
      return
    end
  end

  -- Jeśli nie znaleziono terminala, otwieramy nowy i uruchamiamy komendę
  vim.cmd("botright vnew | terminal")
  vim.fn.chansend(vim.b.terminal_job_id, command .. "\n")
end

function RunCode(mode)
    local exec_command
    local end_message
    local file_type = vim.bo.filetype --current filetype
    local current_file = vim.fn.expand('%:p')
    local buftype = vim.bo.buftype

    if mode == 'nearest_terminal' then
        exec_command = 'ExecuteToTerminal'
        end_message = '  Executed "' .. current_file .. '" in the nearest terminal'

    elseif mode == 'buffer' then
        exec_command = 'ExecuteToWindow'
        end_message = '  Executed "' .. current_file .. '" in a buffer'
    else
        vim.cmd("echo 'O ja cie'")
        return
    end


    if buftype == 'terminal' then
        vim.cmd("echon ' 󰞦 Nie mozna uruchomic kodu na terminalu!'")
        return
    end
    vim.cmd(":silent w")


    if file_type == 'python' then
        vim.cmd("silent " .. exec_command .. " python -B " .. current_file)

    elseif file_type == 'java' then
        local curr_file_no_type = vim.fn.expand('%:r')
        vim.cmd("silent " .. exec_command .. " javac -d class " .. current_file .. " && java -cp class " .. curr_file_no_type)
    --nnoremap <F3> :w <bar> Shell javac % && java % <cr>

    elseif file_type == 'html' then
        vim.cmd("silent !nohup live-server -q > /dev/null &")

    elseif file_type == 'rust' then
        vim.cmd("silent " .. exec_command .. " cargo run")

    elseif file_type == 'scala' then
        vim.cmd("silent " .. exec_command .. " scala3 " .. current_file)

    elseif file_type == 'c' then
        vim.cmd(exec_command .. " gcc -o a " .. current_file .. " && ./a")

    else
        --default case
        local toEcho = "Brak komendy dla pliku: " .. file_type
        vim.cmd("echo '" .. toEcho .. "'")
        return
    end

    vim.cmd("echon '" .. end_message .. "'")

end
