hi clear
syntax reset
let g:colors_name = "mycolorscheme"
set background=light
set t_Co=256
hi Normal guifg=#0d0d0d ctermbg=NONE guibg=#d5d1c5 gui=NONE

hi DiffText guifg=#c61717 guibg=NONE
hi ErrorMsg guifg=#c61717 guibg=NONE
hi WarningMsg guifg=#c61717 guibg=NONE
hi PreProc guifg=#c61717 guibg=NONE
hi Exception guifg=#c61717 guibg=NONE
hi Error guifg=#c61717 guibg=NONE
hi DiffDelete guifg=#c61717 guibg=NONE
hi GitGutterDelete guifg=#c61717 guibg=NONE
hi GitGutterChangeDelete guifg=#c61717 guibg=NONE
hi cssIdentifier guifg=#c61717 guibg=NONE
hi cssImportant guifg=#c61717 guibg=NONE
hi Type guifg=#c61717 guibg=NONE
hi Identifier guifg=#c61717 guibg=NONE
hi PMenuSel guifg=#028a42 guibg=NONE
hi Constant guifg=#028a42 guibg=NONE
hi Repeat guifg=#028a42 guibg=NONE
hi DiffAdd guifg=#028a42 guibg=NONE
hi GitGutterAdd guifg=#028a42 guibg=NONE
hi cssIncludeKeyword guifg=#028a42 guibg=NONE
hi Keyword guifg=#028a42 guibg=NONE
hi IncSearch guifg=#9e8d3f guibg=NONE
hi Title guifg=#9e8d3f guibg=NONE
hi PreCondit guifg=#9e8d3f guibg=NONE
hi Debug guifg=#9e8d3f guibg=NONE
hi SpecialChar guifg=#9e8d3f guibg=NONE
hi Conditional guifg=#9e8d3f guibg=NONE
hi Todo guifg=#9e8d3f guibg=NONE
hi Special guifg=#9e8d3f guibg=NONE
hi Label guifg=#9e8d3f guibg=NONE
hi Delimiter guifg=#9e8d3f guibg=NONE
hi Number guifg=#9e8d3f guibg=NONE
hi CursorLineNR guifg=#9e8d3f guibg=NONE
hi Define guifg=#9e8d3f guibg=NONE
hi MoreMsg guifg=#9e8d3f guibg=NONE
hi Tag guifg=#9e8d3f guibg=NONE
hi String guifg=#9e8d3f guibg=NONE
hi MatchParen guifg=#9e8d3f guibg=NONE
hi Macro guifg=#9e8d3f guibg=NONE
hi DiffChange guifg=#9e8d3f guibg=NONE
hi GitGutterChange guifg=#9e8d3f guibg=NONE
hi cssColor guifg=#9e8d3f guibg=NONE
hi Function guifg=#1d72ae guibg=NONE
hi Directory guifg=#c76bfa guibg=NONE
hi markdownLinkText guifg=#c76bfa guibg=NONE
hi javaScriptBoolean guifg=#c76bfa guibg=NONE
hi Include guifg=#c76bfa guibg=NONE
hi Storage guifg=#c76bfa guibg=NONE
hi cssClassName guifg=#c76bfa guibg=NONE
hi cssClassNameDot guifg=#c76bfa guibg=NONE
hi Statement guifg=#28e8dd guibg=NONE
hi Operator guifg=#28e8dd guibg=NONE
hi cssAttr guifg=#28e8dd guibg=NONE


hi Pmenu guifg=#0d0d0d guibg=#454545
hi SignColumn guibg=#d5d1c5
hi Title guifg=#0d0d0d
hi LineNr guifg=#737373 guibg=#d5d1c5
hi NonText guifg=#c481ff guibg=#d5d1c5
hi Comment guifg=#c481ff gui=italic
hi SpecialComment guifg=#c481ff gui=italic guibg=NONE
hi CursorLine guibg=#454545
hi TabLineFill gui=NONE guibg=#454545
hi TabLine guifg=#737373 guibg=#454545 gui=NONE
hi StatusLine gui=bold guibg=#454545 guifg=#0d0d0d
hi StatusLineNC gui=NONE guibg=#d5d1c5 guifg=#0d0d0d
hi Search guibg=#c481ff guifg=#d5d1c5
hi VertSplit gui=NONE guifg=#454545 guibg=NONE
hi Visual gui=NONE guibg=#454545
