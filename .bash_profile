#
# ~/.bash_profile
#

#[[ -f ~/.bashrc ]] && . ~/.bashrc

###### czyszczenie home
export XDG_CONFIG_HOME=$HOME/.config
export XDG_CACHE_HOME=$HOME/.cache
export XDG_STATE_HOME=$HOME/.local/state
export XDG_DATA_HOME=$HOME/.local/share

export RUSTUP_HOME="$XDG_DATA_HOME"/rustup
export CARGO_HOME="$XDG_DATA_HOME"/cargo

export CUDA_CACHE_PATH="$XDG_CACHE_HOME"/nv
export PARALLEL_HOME="$XDG_CONFIG_HOME"/parallel

#######

export AWT_TOOLKIT=MToolkit
export PATH="$PATH:/home/mcnuggetsx20/bin"
#. "$CARGO_HOME/env"

export JDK_HOME=/usr/lib/jvm/java-23-openjdk
export JAVA_HOME=/usr/lib/jvm/java-23-openjdk


# BEGIN opam configuration
# This is useful if you're using opam as it adds:
#   - the correct directories to the PATH
#   - auto-completion for the opam binary
# This section can be safely removed at any time if needed.
test -r '/home/mcnuggetsx20/.opam/opam-init/init.sh' && . '/home/mcnuggetsx20/.opam/opam-init/init.sh' > /dev/null 2> /dev/null || true
# END opam configuration
#
#### KDE
export QT_QPA_PLATFORMTHEME=qt5ct
export QT_QPA_PLATFORMTHEME=qt6ct
export KDE_FULL_SESSION=false
export QT_STYLE_OVERRIDE=Oxygen

###DMENU
export DMENU_FLAGS="-sb '#D0D0D0' -nf '#f0af16' -sf '#000000' -nb '#1d1b1b' -c -i -l 30"
export DMENU="dmenu_run $DMENU_FLAGS"
export J4_DMENU="j4-dmenu-desktop -d \"dmenu $DMENU_FLAGS\""

